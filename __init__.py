#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .stock import Move
from .cost_manage import CostDistribution, DistributionRule

def register():
    Pool.register(
        CostDistribution,
        DistributionRule,
        Move,
        module='stock_cost_manage', type_='model')